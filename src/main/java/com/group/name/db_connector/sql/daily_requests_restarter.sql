-- scheduled sql via: https://stackoverflow.com/questions/5471080/how-to-schedule-a-job-for-sql-query-to-run-daily
-- use the needed schema.
use hbase;
SET SQL_SAFE_UPDATES = 0;
update userrequestinfo set requestsPerDay=0, recordsPerDay=0;