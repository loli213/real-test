package com.group.name.model.requests;

public class GetRequestModelData {

    private String tableName;

    private String rowKey;

    private String username;

    public GetRequestModelData(String tableName, String rowKey, String username) {
        this.tableName = tableName;
        this.rowKey = rowKey;
        this.username = username;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
