package com.group.name.model.bl;

import com.group.name.model.exceptions.OperationFailedException;
import com.group.name.model.requests.GetRequestModelData;
import com.group.name.model.requests.MultiGetRequestModelData;
import com.group.name.model.response.GetResponseModelData;
import com.group.name.model.response.MultiGetResponseModelData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Business logic of handling Multi-Get request from the client.
 * Contains the limit & Updating the DB
 */
public class MultiGetRequestBL {

    private GetRequestBL getRequestBL;

    public MultiGetRequestBL(GetRequestBL getRequestBL) {
        this.getRequestBL = getRequestBL;
    }


    public ResponseEntity<MultiGetResponseModelData> multiGet(MultiGetRequestModelData multiGetRequestModelData) {
        String tableName = multiGetRequestModelData.getTableName();
        String username = multiGetRequestModelData.getUsername();
        List<String> rowKeys = multiGetRequestModelData.getRowKeys();
        List<GetResponseModelData> getResponseModelDataList = new ArrayList<>();

        /*DBUpdater dbUpdater = DBUpdater.getInstance();
        int effectedRows = dbUpdater.updateRequestNum(rowKeys.size(), username); // update DB.
        System.out.println("Was DB Update succeed? " + (effectedRows == 1));*/


        for (String currentRowKey : rowKeys) {
            try {
                getResponseModelDataList.add(getRequestBL.get(new GetRequestModelData(tableName, currentRowKey, username), false));
            } catch (IOException e) {
                e.printStackTrace();
                throw new OperationFailedException(e.getMessage());
            }
        }
        return new ResponseEntity<>(new MultiGetResponseModelData(getResponseModelDataList), HttpStatus.OK);
    }


}
