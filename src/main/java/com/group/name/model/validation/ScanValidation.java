package com.group.name.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = ScanValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ScanValidation {

    String message() default "Scan Request Validation Was Failed";
    String username();
    String urlToNotify();
    String emailToNotify();
    String startTimestamp();
    String endTimestamp();
    String startRow();
    String endRow();
    String selectedColumns();
    String selectedPrefixColumns();
    String selectedValues();

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
