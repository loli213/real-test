package com.group.name.model.validation;

import com.group.name.model.utils.TimeUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

@Component
public class UserRequestAllow {



    private final static long RECENTLY_HOURS = 2; // todo - define it in a config.prop file.

    public UserRequestAllow() {

    }


    /*
    validation process:
        1) verify that the user exists in the DB.
        2) verify that the user doesn't pass the allowed request.
            * If Not - return true.
            * Otherwise:
                decide if the request can be made by the last date
    */
    public boolean isValid(String username, int requestedRecordsNum) {
        return true;
    }

    /**
     * @param first  first number, as an integer.
     * @param second second number, BigDecimal format.
     * @return true if first less than the second.
     */
    private boolean isAllowed(int first, BigDecimal second) {
        BigDecimal firstBigDec = new BigDecimal(first);
        return firstBigDec.compareTo(second) <= 0;
    }
}
