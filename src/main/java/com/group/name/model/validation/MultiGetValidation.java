package com.group.name.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = MultiGetValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface MultiGetValidation {

    String message() default "No more get requests are allowed";
    String username();
    String rowKeys();

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
