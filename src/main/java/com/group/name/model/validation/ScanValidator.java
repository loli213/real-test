package com.group.name.model.validation;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class ScanValidator implements ConstraintValidator<ScanValidation, Object> {

    private String username;
    private String urlToNotify, emailToNotify;
    private String startTimestamp, endTimestamp;
    private String startRow, endRow;
    private String selectedColumns, selectedPrefixColumns, selectedValues;

    @Override
    public void initialize(ScanValidation constraintAnnotation) {
        this.username = constraintAnnotation.username();
        this.urlToNotify = constraintAnnotation.urlToNotify();
        this.emailToNotify = constraintAnnotation.emailToNotify();
        this.startTimestamp = constraintAnnotation.startTimestamp();
        this.endTimestamp = constraintAnnotation.endTimestamp();
        this.startRow = constraintAnnotation.startRow();
        this.endRow = constraintAnnotation.endRow();
        this.selectedColumns = constraintAnnotation.selectedColumns();
        this.selectedPrefixColumns = constraintAnnotation.selectedPrefixColumns();
        this.selectedValues = constraintAnnotation.selectedValues();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        // TODO: username validation for scan request.

        BeanWrapperImpl beanWrapper = new BeanWrapperImpl(o);
        String usernameValue = (String) beanWrapper.getPropertyValue(username);
        String urlToNotifyValue = (String) beanWrapper.getPropertyValue(urlToNotify);
        String emailToNotifyValue = (String) beanWrapper.getPropertyValue(emailToNotify);
        String startTimestampValue = (String) beanWrapper.getPropertyValue(startTimestamp);
        String endTimestampValue = (String) beanWrapper.getPropertyValue(endTimestamp);
        String startRowValue = (String) beanWrapper.getPropertyValue(startRow);
        String endRowValue = (String) beanWrapper.getPropertyValue(endRow);


        if (urlToNotifyValue == null && emailToNotifyValue == null) {
            setViolationMsg(constraintValidatorContext, "urlToNotify & emailToNotify can't be null together");
            return false;
        }

        if ((startTimestampValue == null || endTimestampValue == null) && startRowValue == null || endRowValue == null) {
            setViolationMsg(constraintValidatorContext,
                    "You have to set at least startTimestamp&endTimestamp or startRow&endRow");
            return false;
        }

        // TODO: timestamp statistic validation
        // TODO: startRow-endRow statistic(?) validation.
        Object tempObj;
        List tempList;
        tempObj = beanWrapper.getPropertyValue(selectedColumns);
        AtomicBoolean isValid = new AtomicBoolean(true);
        if (tempObj != null) {
            tempList = (List) tempObj;
            tempList.forEach(o1 -> {
                String familyAndColumn = (String) o1;
                if (familyAndColumn == null || familyAndColumn.split(":").length != 2) {
                    isValid.set(false);
                }
            });
        }

        if (!isValid.get()) {
            setViolationMsg(constraintValidatorContext,
                    "selectedColumns is not in a valid format. should be [\"columnFamily:columnName\",etc..]");
            return false;
        }

        // todo - validate selectedPrefixColumns & selectedValues
        return true;
    }


    private void setViolationMsg(ConstraintValidatorContext constraintValidatorContext, String msg) {
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext.buildConstraintViolationWithTemplate(msg).addConstraintViolation();
    }


}
